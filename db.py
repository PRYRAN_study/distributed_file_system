import datetime
import mongoengine


class Storage_ip(mongoengine.Document):

    created = mongoengine.DateTimeField(default=datetime.datetime.now)
    file = mongoengine.StringField(required=True,unique=True)
    storage_ips = mongoengine.ListField(required=True)
    size = mongoengine.StringField()
    entry = {
        'db_alias': 'dfs',
        'file': [
            'storage_ips',
            'size'
        ],
        'ordering': ['-created']
    }





class Available(mongoengine.Document):

    available = mongoengine.BooleanField()
    storage_ip = mongoengine.StringField(required=True)

    entry = {
        'db_alias': 'dfs',
        'storage_ip': 'available',
    }



class DFS(mongoengine.Document):

    folder = mongoengine.StringField(required=True)
    list_of_entries = mongoengine.ListField()

    entry = {
        'db_alias': 'dfs',
        'folder': [
            'list_of_entries'
        ]
    }