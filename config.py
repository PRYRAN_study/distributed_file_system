import os
import socket
from pathlib import Path
from threading import Thread
import subprocess

from db import Storage_ip, Available, DFS

def is_folder(node):
    if get_dfs_state_object(node) is None:
        return False
    else:
        return True

# TODO test
def delete_folder_from_DFS(node):
    # parrent_folder = get_path(node)[:-get_name(node)-1]
    # write_to_dfs(parrent_folder,list(set(get_dfs_state(parrent_folder)-set([node]))))
    delete_node_from_DFS(node)
    sub_nodes = get_dfs_state(node)
    for sub_node in sub_nodes:
        delete_folder_from_DFS(sub_node)
        delete_from_storage(sub_node)

    delete_from_dfs(node)

    # folder = Path(node)
    # if folder.exists():
    #     files = folder.iterdir()
    #     for file in files:
    #         if file.is_dir():
    #             delete_folder(file)
    #         else:
    #             delete_node(file)
    #     folder.rmdir()


def get_parent(node):
    return get_path(node)[:-len(get_name(node)) - 1]


def delete_node_from_DFS(node):
    parent = get_parent(node)
    write_to_dfs(parent, get_difference(get_dfs_state(parent), [node]))


def get_difference(list_one, list_two):
    return list(set(list_one) - set(list_two))


def update_dfs_state(file, list_of_storages):
    difference = get_difference(
        get_dfs_state(get_parent(file))
        ,
        list_of_storages
    )
    write_to_dfs()


def get_storage_server(file):
    server = Storage_ip.objects(file=file).first()
    # return server.storage_ips

    if server:
        return server.storage_ips
    else:
        return []


def get_storage_server_object(file):
    server = Storage_ip.objects(file=file).first()
    # return server.storage_ips

    if server:
        return server
    else:
        return []


# add file to dictionary
def write_storage_ip(file, list_of_storage_ips, size):
    s = Storage_ip.objects(file=file).first()
    if not s:
        s = Storage_ip()
        s.file = file
        s.storage_ips = list_of_storage_ips
        s.size = size
        s.save()
    else:
        if size is None:
            Storage_ip.objects(file=file).update_one(storage_ips=list_of_storage_ips)
        else:
            Storage_ip.objects(file=file).update_one(storage_ips=list_of_storage_ips, size__=size)


# delete file from dictionary
def delete_from_storage(file):
    if Storage_ip.objects(file=file):
        Storage_ip.objects(file=file).delete()


#  make storage ip available
def register_storage(storage_ip):
    storage = Available.objects(storage_ip=storage_ip).first()
    if not storage:
        a = Available()
        a.storage_ip = storage_ip
        a.available = True
        a.save()
    else:
        Available.objects(storage_ip=storage_ip).update_one(available=True)


# TRUE if available FALSE if not or doesn`t exist
def is_available(storage_ip):
    storage = Available.objects(storage_ip=storage_ip).first()
    if not storage:
        return False
    else:
        return Available.objects(storage_ip=storage_ip).first().available


# set available=false for failed ip
def failed_storage(storage_ip):
    Available.objects(storage_ip=storage_ip).update_one(available=False)


# get list of files in a folder
def get_dfs_state(folder):
    current = DFS.objects(folder=folder).first()
    if current:
        return current.list_of_entries
    else:
        return []

def get_dfs_state_object(folder):
    current = DFS.objects(folder=folder).first()
    return current


# replace state in folde with given list
def write_to_dfs(folder, list_of_entries):
    current = DFS.objects(folder=folder).first()
    if not current:
        d = DFS()
        d.folder = folder
        d.list_of_entries = list_of_entries
        d.save()
    else:
        DFS.objects(folder=folder).update_one(list_of_entries=list_of_entries)


# delete entry
def delete_from_dfs(folder):
    if DFS.objects(folder=folder):
        DFS.objects(folder=folder).delete()


def get_all_file_mappings():
    return Storage_ip.objects()


def get_path(node):
    return str(node).replace("\\", "/")


def get_name(node):
    return get_path(node).split("/")[-1]

def get_size(node):
    return os.path.getsize(node)

def recieve_from_socket(sock):
    sock.listen(10)
    sc, address = sock.accept()
    buff = sc.recv(BUFFER_SIZE)

    message = bytearray()
    while buff:
        message += buff
        buff = sc.recv(BUFFER_SIZE)
    return message


def tcp_send(ip, port, bytes_to_send):
    s = socket.socket()
    s.connect((ip, port))
    for bytes_batch in chunks(bytes_to_send, BUFFER_SIZE):
        s.send(bytes_batch)
    s.close()


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


def send_file(file, IP, PORT):
    with open(file, "rb") as f:
        tcp_send(IP, PORT, f.read())


def get_file(file, sock):
    with open(file, "wb") as f:
        data = recieve_from_socket(sock)
        f.write(data)


def is_elem_in_list(list, elem):
    return elem in list


# TODO notify user that folder consist of files that going to be deleted
def delete_folder(node):
    folder = Path(node)
    if folder.exists():
        files = folder.iterdir()
        for file in files:
            if file.is_dir():
                delete_folder(file)
            else:
                delete_node(file)
        folder.rmdir()


# asserts that parent folders are created and then creates file
def create_node(node):
    temp_path = ""
    node_parts = node.split("/")
    for i in range(0, len(node_parts) - 1):
        temp_path += node_parts[i] + "/"
        print(temp_path, Path(temp_path).exists())
        if not Path(temp_path).exists():
            Path(temp_path).mkdir()
    Path(temp_path + node_parts[-1]).touch()


def create_folder(node):
    temp_path = ""
    node_parts = node.split("/")
    for i in range(0, len(node_parts)):
        temp_path += node_parts[i] + "/"
        print(temp_path, Path(temp_path).exists())
        if not Path(temp_path).exists():
            Path(temp_path).mkdir()


def delete_node(node):
    file = Path(node)

    if file.exists():
        file.unlink()


def move_node(node, new_path):
    create_node(new_path)
    with open(new_path, "wb") as f:
        with open(node, "rb") as g:
            f.write(g.read())
    delete_node(node)


def add_to_list(list_of_elements, element):
    if not is_elem_in_list(list_of_elements, element):
        list_of_elements.append(element)


def add_if_not_exist(file, list_of_elem):
    list_of_elem = list(list_of_elem)
    if is_elem_in_list(list_of_elem, file):
        return list_of_elem
    else:
        list_of_elem.append(file)
        return list_of_elem


def get_host_ip():
    result = subprocess.run(['curl', '-s', 'http://169.254.169.254/latest/meta-data/public-ipv4'],
                            stdout=subprocess.PIPE)
    return result.stdout.decode('ascii')


# create_folder("/DFS/test/one/two/three")


BUFFER_SIZE = 1024

STORAGE_CONTROL_PORT_NUMBER = 2001
STORAGE_DATA_PORT_NUMBER = 2002

NAME_CONTROL_PORT_NUMBER = 2003
NAME_DATA_PORT_NUMBER = 2004

CLIENT_CONTROL_PORT_NUMBER = 2005
CLIENT_DATA_PORT_NUMBER = 2006

FOLDER_NAME = "DFS"

GUEST_IP = socket.gethostbyname(socket.gethostname())
#GUEST_IP = "10.91.55.174"
#print(GUEST_IP)
HOST_IP = get_host_ip()
#HOST_IP = "10.91.55.174"
