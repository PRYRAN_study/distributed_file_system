import random
from time import sleep
from config import *
from mongo_setup import global_init


def write_to_file(file,message):
    with open(file,"a") as f:
        f.write(message+"\n")


def find_storage_server(file):
    global available_storage_servers
    if len(get_storage_server(file)) == 0:
        return available_storage_servers
    else:
        return get_storage_server(file)

def update():
    global available_storage_servers
    sleep(1)
    keys = [node.file for node in get_all_file_mappings()]
    for i in range(len(keys)):
        if len(get_storage_server(keys[i])) < 3:
            servers_without_file = list(set(available_storage_servers) - set(find_storage_server(keys[i])))

            if len(servers_without_file) > (3 - len(get_storage_server(keys[i]))):
                servers_without_file = servers_without_file[3 - len(get_storage_server(keys[i])):]

            for server_without_file in servers_without_file:
                server_with_file = random.choice(find_storage_server(keys[i]))
                tcp_send(server_without_file, STORAGE_CONTROL_PORT_NUMBER, (HOST_IP + " upload "+ keys[i]).encode())
                tcp_send(server_with_file, STORAGE_CONTROL_PORT_NUMBER, (HOST_IP + " balance "+keys[i] + " " + server_without_file).encode())
                #dfs_file[keys[i]] = dfs_file.get(keys[i]) + servers_without_file
                write_storage_ip(keys[i],add_if_not_exist(server_without_file,get_storage_server(keys[i])),get_storage_server_object(keys[i]).size)
                sleep(3)


def process_command(sc, address):
    global available_storage_servers


    buff = sc.recv(BUFFER_SIZE)
    message = bytearray()
    while buff:
        message+=buff
        buff = sc.recv(BUFFER_SIZE)

    message = message.decode("ascii")

    sender_server = message.split(" ")[0]
    command = message.split(" ")[1]

    create_node("log.txt")
    write_to_file("log.txt",message)
    write_to_file("log.txt",sender_server)
    for i in available_storage_servers:
        write_to_file("log.txt",i)




    if command == "register":
        add_to_list(available_storage_servers,sender_server)
        for i in available_storage_servers:
            write_to_file("log.txt", "in register:"+i)
        print(sender_server)


    else:
        node = message.split(" ")[2]
        requested_storage_servers = find_storage_server(node)
        if command == "update":
            update()
        if command == "upload":
            storage_server = random.choice(requested_storage_servers)
            write_to_file("log.txt", "in upload:"+storage_server)

            size =message.split(" ")[3]

            write_storage_ip(node,
                             add_if_not_exist(storage_server
                                              , get_storage_server(node)
                                              ),size)

            # send storage server request to listen on data port
            tcp_send(storage_server,STORAGE_CONTROL_PORT_NUMBER,message.encode())
            write_to_file("log.txt", "in upload: send tcp to"+storage_server)

            # send client request to send file to storage server ip
            tcp_send(sender_server,CLIENT_CONTROL_PORT_NUMBER,storage_server.encode())
            write_to_file("log.txt", "in upload: send tcp to"+sender_server)




        elif command == "download":
            storage_server = random.choice(requested_storage_servers)
            # send storage server request to send file to client
            tcp_send(storage_server, STORAGE_CONTROL_PORT_NUMBER, message.encode())

        elif command == "delete":
            for storage_server in requested_storage_servers:
                # send storage server request to delete file
                tcp_send(storage_server, STORAGE_CONTROL_PORT_NUMBER, message.encode())

            delete_from_storage(node)


        elif command == "move":
            for storage_server in requested_storage_servers:
                tcp_send(storage_server, STORAGE_CONTROL_PORT_NUMBER, message.encode())
            old_node = message.split(" ")[2]
            new_node = message.split(" ")[3]
            doc = get_storage_server_object(old_node)
            write_storage_ip(new_node,doc.storage_ips,doc.size)

            delete_from_storage(old_node)


        elif command == "create_folder":
            for storage_server in available_storage_servers:
                tcp_send(storage_server, STORAGE_CONTROL_PORT_NUMBER, message.encode())


        elif command == "delete_folder":
            for storage_server in available_storage_servers:
                tcp_send(storage_server, STORAGE_CONTROL_PORT_NUMBER, message.encode())
            delete_from_dfs(node)
        #delay
        sleep(1)
        update()


if __name__ == "__main__":
    control_socket = socket.socket()
    control_socket.bind(("0.0.0.0", NAME_CONTROL_PORT_NUMBER))

    data_socket = socket.socket()
    data_socket.bind(("0.0.0.0", NAME_DATA_PORT_NUMBER))

    available_storage_servers = []

    host_mongo_server_ip = HOST_IP

    db = global_init(host_mongo_server_ip,'ds_db')
    db.drop_database('ds_db')

    update()


    control_socket.listen(10)
    while True:
        sc, address = control_socket.accept()
        thread = Thread(target=process_command, args=(sc,address))
        thread.start()







