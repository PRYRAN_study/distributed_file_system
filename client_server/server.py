import re
from time import sleep

from config import *
from mongo_setup import global_init


def get_prompt():
    print(">", end="")
    promt = input()
    return promt


# client send request to upload file to dfs, name server returns storage server ip and send to that storage server
# request to listen on data socket. Then client send file over data socket to storage server
def request_send_file(node):
    global host_name_server_ip, HOST_IP
    with open(node, "rb") as f:
        global control_socket
        tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER,
                 (HOST_IP + " upload {} {}").format(get_path(node), get_size(node)).encode())
        storage_serve_ip = recieve_from_socket(control_socket)
        # sleep(3)
        tcp_send(storage_serve_ip, STORAGE_DATA_PORT_NUMBER, f.read())


# firstly, client sends request for certain file to name server. Then client starts to listen. On other side,
# name server recieves request from client and sends coresponding storage server request to send certain file to client
def request_get_file(node):
    global host_name_server_ip, HOST_IP
    with open(node, "wb") as f:
        global control_socket, data_socket
        tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER,
                 (HOST_IP + " download {}").format(get_path(node)).encode())

        f.write(recieve_from_socket(data_socket))


def request_delete_file(node):
    global host_name_server_ip, HOST_IP
    delete_node(node)
    tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER, (HOST_IP + " delete {}").format(get_path(node)).encode())



def request_delete_folder(node):
    global host_name_server_ip, HOST_IP
    delete_folder(node)
    tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER,
             (HOST_IP + " delete_folder {}").format(get_path(node)).encode())


def request_move_file(old_node, new_node):
    global FOLDER_NAME
    global host_name_server_ip, HOST_IP
    if new_node.split("/")[0] != FOLDER_NAME:
        print("*WARNING, NEW NODE HAS TO BE IN DFS*")
        return
    else:
        move_node(old_node, new_node)
        tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER,
                 (HOST_IP + " move {} {}").format(old_node, new_node).encode())


def request_create_folder(node):
    global host_name_server_ip, HOST_IP
    tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER, (HOST_IP + " create_folder {}").format(node).encode())


def copy_file_to_buffer(node):
    global buffer
    file = Path(node)
    if file.exists():
        with open(file, "rb") as f:
            buffer = f.read()


def paste_file_from_buffer(node):
    global buffer
    if len(buffer) == 0:
        print("*BUFFER IS EMPTY*")
        return 0
    file = Path(node)
    if file.exists():
        print("*FILE ALREADY EXISTS*\n Want to replace?")
        user_prompt = context_menu(["yes", "no"], only_digit_commands=True)
        user_prompt = int(user_prompt)
        if user_prompt == 0:
            with open(file, "wb") as f:
                f.write(buffer)
        else:
            return 0
    else:
        file.touch()
        with open(file, "wb") as f:
            f.write(buffer)
        return 1


def is_local(node):
    global local_state, path
    return is_elem_in_list([node for node in local_state.get(get_path(path))], node)


def is_at_dfs(node):
    global dfs_state, path
    return is_elem_in_list(dfs_state, node)


def context_menu(options, only_digit_commands):
    i = 0
    output = ""
    for option in options:
        output += "{}. {}\n".format(i, option)
        i += 1
    print(output)
    promt = get_prompt().strip()
    if promt == "":
        return context_menu(options, only_digit_commands)
    elif only_digit_commands:
        # requires only string consists of positive integers with given bounds 0 <= prompt < len(options)
        if not re.fullmatch(r'^-?[0-9]*', promt):
            print("*INVALID INPUT*\n")
            return context_menu(options, only_digit_commands)
        elif int(promt) >= len(options) or int(promt) < 0:
            print("*INVALID COMMAND*\n Please, choose from 0 to {}\n".format(i - 1))

            return context_menu(options, only_digit_commands)
        else:
            return promt.strip()
    else:
        # if integers is given, check its positivity
        if re.fullmatch(r'^-?[0-9]*', promt):
            if int(promt) >= len(options) or int(promt) < 0:
                print("*INVALID COMMAND*\n Please, choose from 0 to {}\n".format(i - 1))
                return context_menu(options, only_digit_commands)
            else:
                return promt.strip()
        else:
            return promt.strip()


def get_info(node, is_at_dfs):
    if is_at_dfs:
        print("servers: ", end="")
        doc = get_storage_server_object(node)
        for server_with_file in doc.storage_ips:
            print(server_with_file, end=" ")
        print(", size: {}".format(doc.size))
        print()
    else:
        print("localy, size: {}".format(get_size(node)))


def file_menu(node, is_local, is_at_dfs):
    print("Context menu of file {}, ".format(get_name(node)))
    get_info(node, is_at_dfs)

    if not is_local and is_at_dfs:

        user_prompt = context_menu(["return", "download from DFS", "delete"], only_digit_commands=True)
        user_prompt = int(user_prompt)

        if user_prompt == 0:
            return
        elif user_prompt == 1:
            request_get_file(node)
        elif user_prompt == 2:
            request_delete_file(node)
            delete_node_from_DFS(node)


    elif is_local and not is_at_dfs:

        user_prompt = context_menu(["return", "load to DFS", "copy", "delete"], only_digit_commands=True)
        user_prompt = int(user_prompt)

        if user_prompt == 0:
            return
        elif user_prompt == 1:
            request_send_file(node)
            write_to_dfs(get_parent(node), add_if_not_exist(node, get_dfs_state(get_parent(node))), )

        elif user_prompt == 2:
            copy_file_to_buffer(node)
        elif user_prompt == 3:
            request_delete_file(node)
            delete_node_from_DFS(node)

    else:
        user_prompt = context_menu(["return", "load to DFS", "download from DFS", "copy", "delete"], only_digit_commands=True)
        user_prompt = int(user_prompt)

        if user_prompt == 0:
            return
        elif user_prompt == 1:
            request_send_file(node)
            write_to_dfs(get_parent(node), add_if_not_exist(node, get_dfs_state(get_parent(node))))
        elif user_prompt == 2:
            request_get_file(node)
        elif user_prompt == 3:
            copy_file_to_buffer(node)
        elif user_prompt == 4:
            request_delete_file(node)
            delete_node_from_DFS(node)


# dict.keys() is consist of all folders in DFS. dict.values is consist of lists of elements in every folder.
def parser(path):
    state = dict()
    stack = []
    stack.append(path)
    while len(stack) != 0:
        current = stack.pop()
        nodes = [x for x in current.iterdir()]
        state[get_path(current)] = [get_path(x) for x in current.iterdir()]
        for node in nodes:
            if node.is_dir(): stack.append(node)
    return state


def init():
    delete_folder(FOLDER_NAME)
    create_folder(FOLDER_NAME)


if __name__ == "__main__":
    create_folder(FOLDER_NAME)


    buffer = bytearray()
    # socket for control messages (commands)
    control_socket = socket.socket()
    control_socket.bind((HOST_IP, CLIENT_CONTROL_PORT_NUMBER))
    # socket for data
    data_socket = socket.socket()
    data_socket.bind((HOST_IP, CLIENT_DATA_PORT_NUMBER))

    # name server ip
    print("Please, write IP of DFS")
    # host_name_server_ip = get_prompt()
    host_name_server_ip = get_prompt()
    host_mongo_server_ip = host_name_server_ip

    global_init(host_mongo_server_ip, 'ds_db')

    path = Path(FOLDER_NAME)
    while True:
        tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER,
                 (HOST_IP + " update ").encode())
        # get current state of DFS
        dfs_state = get_dfs_state(get_path(path))

        # for every folder in DFS, check that corresponding folder is created localy
        for node in dfs_state:
            if not os.path.exists(node) and is_folder(node):
                os.makedirs(node)
        # update FS tree
        local_state = parser(path)
        nodes = local_state.get(get_path(path))

        # get difference DFS tree and local tree
        difference = list(
            set(
                [node for node in dfs_state])
            - set(
                [node for node in local_state.get(get_path(path))])
        )

        # make these nodes that has to be downloaded from DFS appear at user menu
        nodes = nodes + difference

        print("Current directory {}\n".format(path))
        promt = context_menu([".."] + [get_name(node) for node in nodes], only_digit_commands=False)

        if promt.isdecimal():
            promt = int(promt)

            if promt == 0:
                if get_path(path) == FOLDER_NAME:
                    print("*YOU ARE ALREADY AT ROOT*")
                    continue
                else:
                    path = path.parent
            else:
                choice = nodes[promt - 1]
                # if choice is key in local FS tree, then it is folder.
                # note that all folders are automaticaly created. There is no need to check DFS tree for folders
                if is_folder(choice):
                    path = path / get_name(choice)

                else:
                    # define is node only local, only on storage servers or both
                    file_menu(choice,
                              is_local=is_local(choice),
                              is_at_dfs=is_at_dfs(choice))
        else:

            command = promt.split(" ")[0]
            if command == "exit":
                break
            elif command == "create":

                node = get_path(path / promt.split(" ")[1])
                # provided choice: folder or file?
                if not int(context_menu(["folder?", "file?"], only_digit_commands=True)):
                    if not is_local(node):
                        create_folder(node)
                        request_create_folder(node)
                        write_to_dfs(node, [])
                        write_to_dfs(get_parent(node), add_if_not_exist(node, get_dfs_state(get_parent(node))))
                    else:
                        print("*WARNING, FOLDER {} ALREADY EXIST".format(get_name(node)))
                else:
                    # check that file does not exist
                    if not is_local(node):
                        create_node(node)
                    else:
                        print("*WARNING, FILE {} ALREADY EXIST".format(get_name(node)))
            elif command == "delete":
                node = get_path(path / promt.split(" ")[1])

                if len(promt.strip().split(" ")) != 2:
                    print("*INVALID NUMBER OF ARGUMENTS*\n Please, follow format \"paste file_name\"")
                elif is_local(node):
                    if local_state.get(node) is None:
                        # file
                        request_delete_file(node)
                        delete_node_from_DFS(node)
                    elif len(local_state.get(node)) == 0:
                        # folder
                        request_delete_folder(node)
                        delete_folder_from_DFS(node)

                    elif len(local_state.get(node)) != 0:
                        print("*WARNING, FOLDER IS NOT EMPTY!\n Delete nested files and folder?")
                        promt = int(context_menu(["yes", "no"], only_digit_commands=True))
                        if promt == 0:
                            request_delete_folder(node)
                            delete_folder_from_DFS(node)
                    # delay needed to correct display DFS state
                    sleep(0.5)
                else:
                    print("*WARNING, FILE DOES NOT EXITS*\n")
            elif command == "paste":
                if len(promt.strip().split(" ")) != 2:
                    print("*INVALID NUMBER OF ARGUMENTS*\n Please, follow format \"paste file_name\"")
                else:
                    node = get_path(path / promt.split(" ")[1].strip())
                    if paste_file_from_buffer(node):
                        request_send_file(node)
            elif command == "move":
                if len(promt.split(" ")) != 3:
                    print("*INVALID NUMBER OF ARGUMENTS*\n Please, follow format \"move old_node new_node\"")
                else:
                    old_node = get_path(path / promt.split(" ")[1].strip())
                    new_node = promt.split(" ")[2].strip()
                    if not is_local(old_node):
                        print("*WARNING, {} DOES NOT EXIST*".format(get_name(old_node)))
                    elif local_state.get(old_node) is not None:
                        print("*WARNING, {} IS FOLDER".format(get_name(old_node)))
                    elif local_state.get(new_node) is not None:
                        print("*WARNING, {} IS FOLDER".format(new_node))
                    else:
                        # check that file is on dfs
                        if not is_at_dfs(old_node):
                            request_send_file(old_node)
                        request_move_file(old_node, new_node)
                        delete_node_from_DFS(old_node)
                        write_to_dfs(get_parent(new_node),
                                     add_if_not_exist(new_node, get_dfs_state(get_parent(new_node))))
            elif command == "init":
                init()
            else:
                print("*UNRECOGNIZED COMMAND*")
