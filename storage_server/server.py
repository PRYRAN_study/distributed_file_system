import random

from config import *
from mongo_setup import global_init


def process_command(sc):
    buff = sc.recv(BUFFER_SIZE)
    message = bytearray()
    while buff:
        message+=buff
        buff = sc.recv(BUFFER_SIZE)

    message = message.decode("ascii")
    print(message)

    message = message.split(" ")


    client_server = message[0]
    command = message[1]
    node = os.path.join('/', message[2])

    if command == "download":
        send_file(node, client_server, CLIENT_DATA_PORT_NUMBER)
    elif command == "upload":
        global data_socket
        create_node(node)
        get_file(node,data_socket)
    elif command == "move":
        new_node = "/" + message[3]
        move_node(node,new_node)

    elif command == "delete":
        delete_node(node)
    elif command == "delete_folder":
        delete_folder(node)
    elif command == "create_folder":
        create_folder(node)
    elif command == "balance":
        send_file(node, message[3], STORAGE_DATA_PORT_NUMBER)

def write_to_file(file,message):
    with open(file,"a") as f:
        f.write(message+"\n")

def init():
    global  NAME_CONTROL_PORT_NUMBER, host_name_server_ip, HOST_IP
    tcp_send(host_name_server_ip, NAME_CONTROL_PORT_NUMBER, (HOST_IP + " register").encode())

    delete_folder("/"+FOLDER_NAME)
    create_folder("/"+FOLDER_NAME)



    create_node("/DFS/log.txt")
    with open("/DFS/log.txt", "a") as f:
        f.write("storage_ip " + GUEST_IP)
        f.write("name_ip " + host_name_server_ip)

    docs = get_all_file_mappings()
    for doc in docs:
        if is_elem_in_list(doc.storage_ips, HOST_IP):
            if len(doc.storage_ips) == 1:
                delete_from_storage(doc.file)
                delete_node_from_DFS(doc.file)
            else:
                servers_with_file = get_difference(list(doc.storage_ips),[HOST_IP])
                server_with_file = random.choice(servers_with_file)
                tcp_send(server_with_file, STORAGE_CONTROL_PORT_NUMBER,
                         (HOST_IP + " balance " + doc.file + " " + HOST_IP).encode())
                global data_socket
                create_node("/"+doc.file)
                get_file("/"+doc.file, data_socket)


if __name__ == "__main__":
    control_socket = socket.socket()
    control_socket.bind((GUEST_IP, STORAGE_CONTROL_PORT_NUMBER))

    data_socket = socket.socket()
    data_socket.bind((GUEST_IP, STORAGE_DATA_PORT_NUMBER))


    #host_name_server_ip = os.getenv("NAME_IP")
    # storage servers can connect to mongo and name server by its host ip because of routing mesh
    host_name_server_ip = HOST_IP
    host_mongo_server_ip = HOST_IP

    global_init(host_mongo_server_ip,'ds_db')





    # init
    init()


    control_socket.listen(10)
    while True:
        sc, address = control_socket.accept()
        thread = Thread(target=process_command, args=(sc,))
        thread.start()





