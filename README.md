## Install & run


docker images: pryran/na, pryran/st, mongo

login in all remote servers
create & join to docker swarm

run client and connect to name server


code:

# on init node
docker swarm init 

# on others nodes
docker swarm join -- token TOKEN --availability active <ip><portp>

#on init node


docker service create --name mongo \
  --publish published=27017,target=27017 \
  mongo
  
  
docker service create --name name_server \
  --publish published=2003,target=2003\
  --publish published=2004,target=2004 \
  pryran/na:latest



docker service create --name storage_server \
  --publish published=2001,target=2001,mode=host \
  --publish published=2002,target=2002,mode=host \
  --mode global \
  pryran/st:latest

run client_server/server.py
connect to name node
profit



## Diagrams and architecture

![architecture](https://i.imgur.com/cT0Bi3s.png)

![diagram](https://i.imgur.com/5LQeQDP.png)
![diagram](https://i.imgur.com/nldqcXr.png)
![diagram](https://i.imgur.com/iWnXN4q.png)